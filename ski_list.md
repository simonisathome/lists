Skiing Packing List

Flights: 100ml liquid limit

Taxi's

Passenger login

Revolut Money

Music

Passport

Flight tickets

Passenger Locator Form

Plastic document wallet

Covid Passport – NHS App

Masks

Hotel booking

Parking booking

Insurance

Ski pass

Ski rental

Baby wipes

Knife

Fork

Socks

Pants

T-Shirts

Shirts

Leggings

Camelpak / water backpack

Helmet

Goggles

Gloves

Camera

Headphones

Boot carrying handle

Crocs

Ski boots

Ski jacket

Salopettes

Ski socks

Thermals

Boarding boots

Ski wax

Bottle Opener

Plastic food box

Cup or glass

Tesco bag

Sunglasses

Reading glasses

Driving glasses

Ski plastic ID cable

Phone charger

USB cable

Phone battery booster

Pen

Paper

Books

Soap

Shaver

Deoderant

Toothbrush

Toothpaste

Prescriptions

Paracetamol

Imodium

Rennies

Lipsil

Suncream
